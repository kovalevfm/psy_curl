
#include "cpsy_curl.h"

long sys_page_size;

static inline inline int mem_chunk_new(mem_chunk_t ** chunk, size_t len){
    size_t tmp_size = (((sizeof(mem_chunk_t) + len) / sys_page_size) + 1) * sys_page_size;
    if(!*chunk){
        *chunk = malloc(tmp_size);
        if(!*chunk)
            return -1;
    }
    else{
        (*chunk)->next = malloc(tmp_size);
        if(!(*chunk)->next)
            return -1;
        *chunk = (*chunk)->next;
    }

    (*chunk)->size = tmp_size - sizeof(mem_chunk_t);
    (*chunk)->used = 0;
    (*chunk)->next = NULL;

    return 0;
}

void mem_chunk_free(mem_chunk_t * chunk){
    if(!chunk)
        return;

    while(chunk){
        mem_chunk_t * next = chunk->next;
        free(chunk);
        chunk = next;
    }
}

static inline size_t mem_chunk_write(mem_chunk_t ** chunk, char * data, size_t len){
    size_t tmp_size;
    size_t ret = 0;
    char * c_start = data;
    char * c_end = data + len;

    while(c_start < c_end){
        tmp_size = ((*chunk)->size - (*chunk)->used);
        if(tmp_size <= 0){
            if(mem_chunk_new(chunk, c_end - c_start) == -1)
                return -1;
            tmp_size = ((*chunk)->size - (*chunk)->used);
        }

        tmp_size = MIN(c_end - c_start, tmp_size);
        memcpy((*chunk)->data + (*chunk)->used, c_start, tmp_size);
        (*chunk)->used += tmp_size;
        c_start += tmp_size;
        ret += tmp_size;
    }
    return ret;
}


/*
 * Curl debug callback
 * CURLOPT_DEBUGFUNCTION
 */
/*
static int curl_debug_cb (CURL * eh, curl_infotype type, char * data, size_t d_size, void * userp){
    int ret;
    debug_chunk_t * chunk;
    return 0;
}
*/


/*
 * Curl read callback
 * CURLOPT_READFUNCTION
 */
static size_t read_callback(void *ptr, size_t size, size_t nmemb, void *userp){
  struct tcurl_task_t * task = (struct tcurl_task_t *)userp;
  size_t realsize = size * nmemb;
  size_t read_size = MIN(realsize, task->read_left);

  memcpy((char *)ptr, task->read_data, read_size);
  task->read_left -= read_size;
  return read_size;
}

/*
 * Curl write callback
 * CURLOPT_WRITEFUNCTION
 */
static size_t
curl_write_cb(void *contents, size_t size, size_t nmemb, void *userp)
{
    size_t realsize = size * nmemb;

    tcurl_task_t * task = (tcurl_task_t * )userp;

    if(task->rsp_data_head == NULL){
        // XXX allocate first chunk using Content-Size size.
        if(mem_chunk_new(&task->rsp_data_head, realsize) == -1)
            // this will tell curl we failed to write
            // and easy curl thus will also fail.
            return 0;
        task->rsp_data_cur = task->rsp_data_head;
    }

    if(mem_chunk_write(&task->rsp_data_cur, contents, realsize) == -1)
        return 0;

    task->rsp_size += realsize;

    return realsize;
}


/*
 * Curl header write callback
 * CURLOPT_HEADERFUNCTION
 */
static size_t
curl_header_write_cb(void *contents, size_t size, size_t nmemb, void *userp)
{
    size_t realsize = size * nmemb;
    struct tcurl_task_t * task = (struct tcurl_task_t *)userp;

    int buf_offset = 0;
    if(task->headers_cnt){
        buf_offset = task->headers_offset[task->headers_cnt - 1];
    }

    // can't fit header
    if(realsize + buf_offset >= PSYC_MAX_HEADERS_SIZE || task->headers_cnt + 1 >= PSYC_MAX_HEADERS ){
        return -1;
    }
    memcpy( (char *)(&task->headers) + buf_offset, contents, realsize);
    task->headers_offset[task->headers_cnt] = realsize + buf_offset;
    task->headers_cnt += 1;

    if(task->should_inflate){
        char * colon_index = memchr((char *)(&task->headers) + buf_offset, 58, realsize);
        if(colon_index){
            if(strcasestr( (char *)(&task->headers) + buf_offset, "Content-Encoding") < colon_index){
                // XXX error here
                if(strcasestr((char *)(&task->headers) + buf_offset, "gzip")){
                    task->should_inflate++;
                }
            }
        }
    }

    return realsize;
}


/*
 * should be called when out_signal_fd indicates
 * that there are complete tasks in thread out queue
 */
int psycurl_get_complete(thread_work_info_t * self, tcurl_task_t ** result){
    int ret;
    char buf[SIGNAL_BUF_SIZE];

    /*
     * if the data is still on pipe
     * this will just cause another call to psycurl_get_complete
     */
    ret = read(self->out_signal_fd[0], buf, SIGNAL_BUF_SIZE);
    if(ret == -1 && errno != EAGAIN && errno != EINTR)
        return errno;


    ret = pthread_mutex_lock(&self->out_lock);
    if(ret != 0)
        return ret;

    *result = self->rsp_queue_head;
    self->rsp_queue_head = NULL;
    self->rsp_queue_tail = NULL;
    self->rsp_queue_len = 0;

    return pthread_mutex_unlock(&self->out_lock);
}


int psycurl_put_task(thread_work_info_t * self, tcurl_task_t * new_task){
    int ret;

    // sanity check
    new_task->next = NULL;

    ret = pthread_mutex_lock(&self->in_lock);
    if(ret != 0){
        return ret;
    }

    if(self->in_task_queue_len > self->in_queue_max_len){
        // no room in queue
        pthread_mutex_unlock(&self->in_lock);
        errno = EAGAIN;
        return -1;
    }
    if(!self->in_task_queue){
        // empty task list
        self->in_task_queue = new_task;
        self->in_task_queue_tail = new_task;
    }
    else{
        // append new
        self->in_task_queue_tail->next = new_task;
        self->in_task_queue_tail = self->in_task_queue_tail->next;
    }
    self->in_task_queue_len += 1;

    pthread_mutex_unlock(&self->in_lock);

    /*
     * write return code does not matter
     * only locking matters
     * Note: no race here - if worker thread
     * reads queue before receive signal on pipe
     * this is ok.
     */
    ret = write(self->in_signal_fd[1], "\n", 1);
    if(ret == -1 && errno != EAGAIN && errno != EINTR)
        return errno;
    return 0;
}


static inline int
read_in_queue(thread_work_info_t * self,
              int * q_size,
              tcurl_task_t ** in_queue_head,
              tcurl_task_t ** in_queue_tail
              ){

    char buf[SIGNAL_BUF_SIZE];

    int read_ret = read(self->in_signal_fd[0], buf, SIGNAL_BUF_SIZE);
    if(read_ret > 0){
        // lock in queue & read in tasks
        int ret = pthread_mutex_lock(&self->in_lock);
        if(ret != 0)
            return ret;

        if(!(*in_queue_head)){
            *in_queue_head = self->in_task_queue;
        }
        else{
            (*in_queue_tail)->next = self->in_task_queue;
        }
        *in_queue_tail = self->in_task_queue_tail;

        *q_size += self->in_task_queue_len;

        // reset queue on thread_work_info_t
        self->in_task_queue = NULL;
        self->in_task_queue_tail = NULL;
        self->in_task_queue_len = 0;

        pthread_mutex_unlock(&self->in_lock);
    }

    if(read_ret == -1 && errno != EAGAIN && errno != EINTR)
        return errno;

    return 0;
}


static inline int decompress_body(thread_work_info_t * self, tcurl_task_t * task){
    int zret;
    unsigned have;
    z_stream strm;
    char infl_buf[GZIP_INFLATE_BUF];

    size_t size_ret;
    size_t infalted_size = 0;

    mem_chunk_t * in_chunk = task->rsp_data_head;
    mem_chunk_t * out_chunk_cur = NULL;
    mem_chunk_t * out_chunk_head = NULL;

    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;
    strm.opaque = Z_NULL;
    strm.avail_in = 0;
    strm.next_in = Z_NULL;

    zret = inflateInit2(&strm, MAX_WBITS + 16);
    if (zret != Z_OK)
        return zret;

    while(1){
        if(!in_chunk)
            break;
        strm.avail_in = in_chunk->used;
        strm.next_in = (unsigned char *)in_chunk->data;
        in_chunk = in_chunk->next;

        /* run inflate() on input */
        do {
            strm.avail_out = GZIP_INFLATE_BUF;
            strm.next_out = (unsigned char *)infl_buf;

            // zret = inflate(&strm, Z_SYNC_FLUSH);
            zret = inflate(&strm, Z_NO_FLUSH);
            if(zret != Z_OK && zret != Z_STREAM_END)
                goto inflate_err;

            have = GZIP_INFLATE_BUF - strm.avail_out;

            if(out_chunk_head == NULL){
                if(mem_chunk_new(&out_chunk_head, have) == -1)
                    goto inflate_err;
                out_chunk_cur = out_chunk_head;
            }

            size_ret = mem_chunk_write(&out_chunk_cur, infl_buf, have);
            if(size_ret == -1)
                goto inflate_err;
            infalted_size += size_ret;

            if(zret == Z_STREAM_END){
                break;
            }
        } while (strm.avail_out == 0);

        if(zret == Z_STREAM_END){
            if(in_chunk){
                errno = ENODATA;
                goto inflate_err;
            }
            break;
        }
    }

    (void)inflateEnd(&strm);

    /* free gzipped data */
    mem_chunk_free(task->rsp_data_head);

    task->rsp_size = infalted_size;
    task->rsp_data_head = out_chunk_head;
    task->rsp_data_cur = out_chunk_cur;

    return 0;
inflate_err:
    (void)inflateEnd(&strm);

    /* free partially infalted data  */
    mem_chunk_free(out_chunk_head);

    return -1;
}


static inline int
put_task_in_rsp_queue(thread_work_info_t * self, tcurl_task_t * task){

    int ret = pthread_mutex_lock(&self->out_lock);
    if(ret != 0)
        // self->sys_errno = ret;
        // goto cleanup_wthread;
        return errno;

    if(self->rsp_queue_head == NULL){
        // rsp list empty
        self->rsp_queue_head = task;
        self->rsp_queue_tail = task;
    }
    else{
        self->rsp_queue_tail->next = task;
        self->rsp_queue_tail = self->rsp_queue_tail->next;
    }
    self->rsp_queue_len += 1;

    pthread_mutex_unlock(&self->out_lock);

    ret = write(self->out_signal_fd[1], "\n", 1);
    if(ret == -1 && errno != EAGAIN && errno != EINTR)
        return errno;

    return 0;

}


/*
 * thread worker function
 */

static void * worker_routine(void * arg){

    thread_work_info_t * self = arg;

    CURLM *cm;

    struct curl_waitfd extra_wait_fds[2];

    // thread queue
    tcurl_task_t *in_queue_head = NULL;
    tcurl_task_t *in_queue_tail = NULL;
    tcurl_task_t * task_tmp;
    int in_queue_len = 0;

    CURLMcode mcurl_err;
    CURLcode curl_ret;
    CURLMcode curl_m_ret;

    tcurl_easy_ll_t free_curls[self->curls_num];
    tcurl_easy_ll_t * f_curl_list;
    int free_curls_len = self->curls_num;
    tcurl_easy_ll_t * f_curl;

    int tmp, ret;
    tcurl_task_t * tmp_task;

    memset(&extra_wait_fds, 0x00, sizeof(struct curl_waitfd) * 2);

    cm = curl_multi_init();
    if(!cm)
        goto wthread_exit;

    /*
    curl_multi_setopt(cm, CURLMOPT_MAXCONNECTS, (long)self->maxconns);
    curl_multi_setopt(cm, CURLMOPT_PIPELINING, 0);
    curl_multi_setopt(cm, CURLMOPT_MAX_HOST_CONNECTIONS, 100);
    curl_multi_setopt(cm, CURLMOPT_MAX_TOTAL_CONNECTIONS, 100);
    */

    f_curl_list = free_curls;
    for(int i = 0; i < self->curls_num; ++i){
        free_curls[i].eh = curl_easy_init();
        if(!free_curls[i].eh)
            goto wthread_exit;

        free_curls[i].in_multi = 0;
        free_curls[i].next = &free_curls[i+1];
        free_curls[i].cur_task = NULL;
    }
    free_curls[self->curls_num - 1].next = NULL;

    // exit signal pipe
    extra_wait_fds[0].fd = self->exit_signal_fd[0];
    extra_wait_fds[0].events = CURL_WAIT_POLLIN;

    // in queue signal pipe
    extra_wait_fds[1].fd = self->in_signal_fd[0];
    extra_wait_fds[1].events = CURL_WAIT_POLLIN;

    self->thread_running = 1;

    int numfds;
    long poll_timeout;
    while(1){
        // thread main loop

        curl_m_ret = curl_multi_timeout(cm, &poll_timeout);
        if(curl_m_ret != CURLM_OK){
            self->curl_m_exit_code = curl_m_ret;
            break;
        }

        /*
         * if poll_timeout == 0 we should
         * immediately call perform()
         */
        if(poll_timeout){
            if(poll_timeout < 0 ){
                poll_timeout = DEFAULT_POLL_TIMEOUT;
            }
            else{
                poll_timeout = MIN(poll_timeout, DEFAULT_POLL_TIMEOUT);
            }

            if(free_curls_len && in_queue_len < free_curls_len && self->rsp_queue_len < self->out_queue_max_len){
                // read new tasks
                mcurl_err = curl_multi_wait(cm, extra_wait_fds, 2, poll_timeout, &numfds);
            }
            else{
                // just poll and wait for exit command
                mcurl_err = curl_multi_wait(cm, extra_wait_fds, 1, poll_timeout, &numfds);
            }

            if(mcurl_err != CURLM_OK){
                self->curl_m_exit_code = mcurl_err;
                break;
            }
        }

        if(extra_wait_fds[0].revents){
            // exit signal
            break;
        }

        if( extra_wait_fds[1].revents && free_curls_len && in_queue_len < free_curls_len){
            ret = read_in_queue(self, &in_queue_len, &in_queue_head, &in_queue_tail);
            if(ret != 0){
                self->sys_errno = ret;
                break;
            }
        }


        /*
         * process in queue while queue and free curls
         */
        while(in_queue_len > 0 && free_curls_len > 0){

            // take free curl
            f_curl = f_curl_list;

            // reset selected handle
            curl_easy_reset(f_curl->eh);
            f_curl->cur_task = in_queue_head;

            if((curl_ret = curl_easy_setopt(f_curl->eh, CURLOPT_PRIVATE, f_curl)) != CURLE_OK)
                goto setopt_error;


            // curl_easy_setopt(f_curl->eh, CURLOPT_VERBOSE, 1);
            // curl_easy_setopt(f_curl->eh, CURLOPT_FORBID_REUSE, 1);
            // curl_easy_setopt(f_curl->eh, CURLOPT_FRESH_CONNECT, 1);


            tcurl_option_t * set_option = in_queue_head->options;
            while(set_option){
                // XXX skip if option is private
                // XXX check if timeout option set!
                if(set_option->p_type == LongParam){
                    if((curl_ret = curl_easy_setopt(f_curl->eh, set_option->option, set_option->l_param)) != CURLE_OK)
                        goto setopt_error;
                    // if(set_option->option == CURLOPT_VERBOSE)
                }
                else if(set_option->p_type == CharPtrParam){
                    if(set_option->option == CURLOPT_READDATA){
                        if((curl_ret = curl_easy_setopt(f_curl->eh, CURLOPT_READDATA, in_queue_head)) != CURLE_OK)
                            goto setopt_error;

                        if((curl_ret = curl_easy_setopt(f_curl->eh, CURLOPT_READFUNCTION, read_callback)) != CURLE_OK)
                            goto setopt_error;

                        if((curl_ret = curl_easy_setopt(f_curl->eh, CURLOPT_POSTFIELDSIZE, in_queue_head->read_left)) != CURLE_OK)
                            goto setopt_error;
                    }
                    else{
                        if((curl_ret = curl_easy_setopt(f_curl->eh, set_option->option, set_option->c_param)) != CURLE_OK)
                            goto setopt_error;
                    }
                }
                else if(set_option->p_type == SlistParam){
                    if((curl_ret = curl_easy_setopt(f_curl->eh, set_option->option, set_option->slist_param)) != CURLE_OK)
                        goto setopt_error;
                }
                set_option = set_option->next;
            }


            if((curl_ret = curl_easy_setopt(f_curl->eh, CURLOPT_WRITEFUNCTION, curl_write_cb)) != CURLE_OK)
                goto setopt_error;

            if((curl_ret = curl_easy_setopt(f_curl->eh, CURLOPT_WRITEDATA, (void *)in_queue_head)) != CURLE_OK)
                goto setopt_error;

            if((curl_ret = curl_easy_setopt(f_curl->eh, CURLOPT_HEADERFUNCTION, curl_header_write_cb)) != CURLE_OK)
                goto setopt_error;

            if((curl_ret = curl_easy_setopt(f_curl->eh, CURLOPT_HEADERDATA, (void *)in_queue_head)) != CURLE_OK)
                goto setopt_error;

            // options set.. add handle to multi
            curl_m_ret = curl_multi_add_handle(cm, f_curl->eh);
            if(curl_m_ret != CURLM_OK){
                self->curl_m_exit_code = curl_m_ret;
                goto cleanup_wthread;
            }

            f_curl->in_multi = 1;

            // consume task from in queue
            in_queue_head = in_queue_head->next;
            in_queue_len--;

            // decouple task from queue just in case
            f_curl->cur_task->next = NULL;

            // consume free curl
            f_curl_list = f_curl_list->next;
            f_curl->next = NULL;
            free_curls_len--;

            // proceed with next task in queue
            continue;

            setopt_error:
                // put task in respose queue
                task_tmp = in_queue_head;

                in_queue_head = in_queue_head->next;
                in_queue_len--;

                task_tmp->curl_ret = curl_ret;
                task_tmp->next = NULL;

                if((self->sys_errno = put_task_in_rsp_queue(self, task_tmp)) != 0)
                    goto cleanup_wthread;

        }
        // end in_queue processing


        // perform
        curl_m_ret = curl_multi_perform(cm, &tmp);
        if(curl_m_ret != CURLM_OK){
            self->curl_m_exit_code = curl_m_ret;
            break;
        }

        // read complete
        CURLMsg *msg;
        int got_responses = 0;
        while ((msg = curl_multi_info_read(cm, &tmp))) {

            got_responses += 1;

            char * c_tmp;
            CURL *e = msg->easy_handle;

            // curl_easy_getinfo wants (char **) as third argument
            if((curl_ret = curl_easy_getinfo(e, CURLINFO_PRIVATE, (char **)&f_curl)) != CURLE_OK)
                goto cleanup_wthread;

            tcurl_task_t * complete_task = f_curl->cur_task;

            curl_m_ret = curl_multi_remove_handle(cm, e);
            if(curl_m_ret != CURLM_OK){
                self->curl_m_exit_code = curl_m_ret;
                goto cleanup_wthread;
            }
            f_curl->in_multi = 0;

            // insert f_curl back to free_curl_list
            f_curl->next = f_curl_list;
            f_curl_list = f_curl;
            free_curls_len++;

            complete_task->curl_ret = msg->data.result;
            if(complete_task->curl_ret == CURLE_OK){
                complete_task->curl_ret = curl_easy_getinfo(e, CURLINFO_RESPONSE_CODE, &complete_task->response_code);
                if(complete_task->curl_ret != CURLE_OK)
                    goto info_read_done;


                complete_task->curl_ret = curl_easy_getinfo(e, CURLINFO_EFFECTIVE_URL, &c_tmp);
                if(complete_task->curl_ret != CURLE_OK)
                    goto info_read_done;


                complete_task->effective_url = strdup(c_tmp);
                if(complete_task->effective_url == NULL){
                    complete_task->sys_errno = errno;
                    goto info_read_done;
                }

                complete_task->curl_ret = curl_easy_getinfo(e, CURLINFO_TOTAL_TIME, &complete_task->total_time);
                if(complete_task->curl_ret != CURLE_OK)
                    goto info_read_done;

                complete_task->curl_ret = curl_easy_getinfo(e, CURLINFO_NAMELOOKUP_TIME, &complete_task->namelookup_time);
                if(complete_task->curl_ret != CURLE_OK)
                    goto info_read_done;

                complete_task->curl_ret = curl_easy_getinfo(e, CURLINFO_CONNECT_TIME, &complete_task->connect_time);
                if(complete_task->curl_ret != CURLE_OK)
                    goto info_read_done;

                complete_task->curl_ret = curl_easy_getinfo(e, CURLINFO_PRIMARY_IP, &c_tmp);
                if(complete_task->curl_ret != CURLE_OK)
                    goto info_read_done;


                complete_task->primary_ip = strdup(c_tmp);
                if(complete_task->primary_ip == NULL){
                    complete_task->sys_errno = errno;
                    goto info_read_done;
                }

                complete_task->curl_ret = curl_easy_getinfo(e, CURLINFO_PRIMARY_PORT, &complete_task->primary_port);
                if(complete_task->curl_ret != CURLE_OK)
                    goto info_read_done;


                /* task->should_inflate == 1 means intention to inflate */
                if(complete_task->should_inflate > 1){
                    if(decompress_body(self, complete_task) == -1){
                        // XXX set zlib error on task
                        // printf("decompress_body error\n");
                        goto info_read_done;
                    }
                }

                // success response only if we here
                complete_task->success = 1;
            }
            info_read_done:
                // put task in rsp queue
                if((self->sys_errno = put_task_in_rsp_queue(self, complete_task)) != 0)
                    goto cleanup_wthread;
                f_curl->cur_task = NULL;
        }
        // end curl_multi_info_read()

        // notify main thread
        if(got_responses){
            ret = write(self->out_signal_fd[1], "\n", 1);
        }

    }
    // end thread main loop

cleanup_wthread:

    self->thread_running = 0;

    // read in queue
    ret = read_in_queue(self, &in_queue_len, &in_queue_head, &in_queue_tail);
    // to prevent futher writing to in queue.
    self->in_queue_max_len = -1;

    /*
     * put all undone tasks in out queue
     * main thread will free them
     */
    tmp_task = in_queue_head;
    while(tmp_task){
        tmp_task->sys_errno = EINTR;
        tmp_task = tmp_task->next;
    }
    ret = pthread_mutex_lock(&self->out_lock);
        if(ret != 0){
            // get rid of this goto
            goto wthread_exit;
        }
        if(in_queue_head){
            if(self->rsp_queue_head == NULL){
                // rsp queue empty
                self->rsp_queue_head = in_queue_head;
                self->rsp_queue_tail = in_queue_tail;
            }
            else{
                self->rsp_queue_tail->next = in_queue_head;
                self->rsp_queue_tail = in_queue_tail;
            }
            self->rsp_queue_len += in_queue_len;
        }
        // add responses from incomplete transfers
        for(int i = 0; i < self->curls_num; ++i){
            if(free_curls[i].cur_task){
                if(self->rsp_queue_head == NULL){
                    self->rsp_queue_head = free_curls[i].cur_task;
                    self->rsp_queue_tail = self->rsp_queue_head;
                }
                else{
                    self->rsp_queue_tail->next = free_curls[i].cur_task;
                    self->rsp_queue_tail = self->rsp_queue_tail->next;
                }
                self->rsp_queue_tail->sys_errno = EINTR;
                self->rsp_queue_tail->next = NULL;
                self->rsp_queue_len += 1;
            }
        }
    pthread_mutex_unlock(&self->out_lock);

wthread_exit:

    self->thread_running = 0;

    // cleanup curls
    for(int i = 0; i < self->curls_num; ++i){
        if(free_curls[i].in_multi){
            curl_multi_remove_handle(cm, free_curls[i].eh);
        }
        curl_easy_cleanup( free_curls[i].eh );
    }
    curl_multi_cleanup(cm);


    // signal to main thread so that it can
    // read cleaned up tasks
    ret = write(self->out_signal_fd[1], "\n", 1);

    pthread_exit(0);
    return 0;
}

/*
 * used to start new worker thread
 * returns NULL on error
 */
thread_work_info_t * psycurl_start_thread(int curls_num,
        long maxconns,
        int in_queue_size,
        int out_queue_size){
    int ret;
    thread_work_info_t * self;

    // global initization
    if(!sys_page_size){
        sys_page_size = sysconf(_SC_PAGE_SIZE);
    }

    self = calloc(1, sizeof(thread_work_info_t));
    if(!self){
        goto err1;
    }
    // memset(self, 0x00, sizeof(thread_work_info_t));

    self->curls_num = curls_num;
    self->maxconns = maxconns;

    self->in_task_queue = NULL;
    self->in_task_queue_tail = NULL;
    self->in_task_queue_len = 0;
    self->in_queue_max_len = in_queue_size;

    self->rsp_queue_head = NULL;
    self->rsp_queue_tail = NULL;
    self->rsp_queue_len = 0;
    self->out_queue_max_len = out_queue_size;

    self->curl_m_exit_code = 0;
    self->curl_exit_code = 0;
    self->sys_errno = 0;

    ret = pipe2(self->in_signal_fd, O_NONBLOCK);
    if(ret != 0){
        goto err2;
    }
    ret = pipe2(self->out_signal_fd, O_NONBLOCK);
    if(ret != 0){
        goto err3;
    }
    ret = pipe2(self->exit_signal_fd, O_NONBLOCK);
    if(ret != 0){
        goto err4;
    }


    if( pthread_mutex_init(&self->in_lock, NULL) != 0){
        goto err5;
    }
    if( pthread_mutex_init(&self->out_lock, NULL) != 0){
        goto err6;
    }

    ret = pthread_create (&self->thread, NULL, worker_routine, self);
    if(ret != 0){
        goto err7;
    }

    return self;

err7:
    pthread_mutex_destroy(&self->out_lock);
err6:
    pthread_mutex_destroy(&self->in_lock);
err5:
    close(self->exit_signal_fd[0]);
    close(self->exit_signal_fd[1]);
err4:
    close(self->out_signal_fd[0]);
    close(self->out_signal_fd[1]);
err3:
    close(self->in_signal_fd[0]);
    close(self->in_signal_fd[1]);
err2:
    free(self);
err1:
    return NULL;
}


void psycurl_stop_thread(thread_work_info_t * self){
    int ret __attribute__ ((unused));
    ret = write(self->exit_signal_fd[1], "\n", 1);
    ret = pthread_join(self->thread, NULL);
}


void psycurl_free_thread(thread_work_info_t * self){

    if(!self){
        return;
    }

    close(self->in_signal_fd[0]);
    close(self->in_signal_fd[1]);

    close(self->out_signal_fd[0]);
    close(self->out_signal_fd[1]);

    close(self->exit_signal_fd[0]);
    close(self->exit_signal_fd[1]);

    pthread_mutex_destroy(&self->in_lock);
    pthread_mutex_destroy(&self->out_lock);

    free(self);
}


